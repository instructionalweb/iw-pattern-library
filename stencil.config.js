exports.config = {
  bundles: [{
    components: ['iw-docs', 'iw-docs-page']
  },
  {
    components: ['iw-breadcrumbs']
  },
  {
    components: ['iw-dropdown-menu']
  },
  {
    components: ['iw-mega-menu']
  },
  {
    components: ['iw-tled-logo', 'iw-acc-logo']
  },
  {
    components: ['iw-accordion']
  },
  {
    components: ['iw-accordion-menu']
  },
  {
    components: ['iw-modal']
  },
  {
    components: ['iw-alert']
  },
  {
    components: ['iw-code']
  },
  {
    components: ['iw-icon-search', 'iw-icon-fb', 'iw-icon-twitter', 'iw-icon-instagram', 'iw-icon-home', 'iw-icon-phone', 'iw-icon-location', 'iw-icon-clock', 'iw-icon-access', 'iw-icon-power', 'iw-icon-bars', 'iw-icon-star-empty', 'iw-icon-star-half', 'iw-icon-star-full', 'iw-icon-warning', 'iw-icon-notification', 'iw-icon-plus', 'iw-icon-minus', 'iw-icon-info', 'iw-icon-blocked', 'iw-icon-x', 'iw-icon-circle-up', 'iw-icon-circle-down']
  }
  ],
  collections: [{
    name: '@stencil/router'
  }],
  publicPath: 'https://s3-us-west-2.amazonaws.com/instruction.austincc.edu/components',
  buildDir: 'components',
  serviceWorker: {
    modifyUrlPrefix: {
      'components/': 'https://s3-us-west-2.amazonaws.com/instruction.austincc.edu/components/'
    }
  }
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};