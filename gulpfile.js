var gulp = require('gulp');
var gutil = require('gulp-util');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var source = require('vinyl-source-stream');
var globby = require('globby');
var browserify = require('browserify');
var webpack = require('webpack');
var gulpWebpack = require('gulp-webpack');
var babelify = require('babelify');
var named = require('vinyl-named');
var gulpif = require('gulp-if');
var tap = require('gulp-tap');
var path = require('path');
var through = require('through2');
var nunjucks = require('gulp-nunjucks');

// we probably dont need sourcemaps yet
// var sourcemaps = require('gulp-sourcemaps');

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'js', 'html'], function() {
    browserSync.init({
        server: './dist',
        port: '3333',
        browser: 'google chrome canary'
    });

    gulp.watch(['src/scss/**/*.scss', 'src/docs/**/*.scss'], ['sass']);
    gulp.watch('src/**/*.html', ['html']);
    gulp.watch(['src/**/*.js', 'src/**/*.spec.js'], ['js']);
    gulp.watch('dist/**/*.*').on('change', browserSync.reload);
});

gulp.task('js', function() {
    gulp
        .src(['src/js/*.js', 'src/docs/**/*.js', '!src/docs/**/*.spec.js'])
        .pipe(named())
        .pipe(
            gulpWebpack({
                    module: {
                        loaders: [{
                            test: /\.js?$/,
                            loader: 'babel-loader'
                        }]
                    }
                },
                webpack
            )
        )
        .pipe(gulp.dest('dist/js'));
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp
        .src(['src/scss/style.scss'])
        .pipe(
            sass({
                outputStyle: 'expanded',
                includePaths: ['src/scss', 'src/docs', 'node_modules']
            }).on('error', sass.logError)
        )
        .pipe(
            autoprefixer({
                //browsers: ['last 2 versions'],
                browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
                cascade: true
            })
        )
        .pipe(gulp.dest('www/css'))
        .pipe(browserSync.stream());
});

gulp.task('html', function() {
    gulp
        .src(['src/index.html'])
        .pipe(nunjucks.compile())
        .pipe(gulp.dest('dist'));
});

gulp.task('docs', function() {
    gulp.src('src/docs/*.html').pipe(gulp.dest('www/docs'));
});

gulp.task('docstyle', function() {
    gulp.src(['src/scss/docs.scss'])
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: ['src/scss', 'src/docs', 'node_modules']
        }).on('error', sass.logError))
        .pipe(
            autoprefixer({
                //browsers: ['last 2 versions'],
                browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
                cascade: true
            })
        )
        .pipe(gulp.dest('www/css'));
});

gulp.task('watchDocs', function() {
    gulp.watch(['src/scss/docs.scss', 'src/scss/docs/*.scss'], ['docstyle']);
    gulp.watch('src/docs/*.html', ['docs']);
});


gulp.task('default', ['serve']);