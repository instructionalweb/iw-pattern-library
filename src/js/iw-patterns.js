import jQuery from 'jquery';
window.jQuery = jQuery;
window.$ = jQuery;

import {
    Foundation
} from 'foundation-sites/js/foundation.core';
Foundation.addToJquery(jQuery);

import {
    rtl,
    GetYoDigits,
    transitionend
} from 'foundation-sites/js/foundation.util.core';
Foundation.rtl = rtl;
Foundation.GetYoDigits = GetYoDigits;
Foundation.transitionend = transitionend;

import {
    Box
} from 'foundation-sites/js/foundation.util.box';
import {
    onImagesLoaded
} from 'foundation-sites/js/foundation.util.imageLoader';
import {
    Keyboard
} from 'foundation-sites/js/foundation.util.keyboard';
import {
    MediaQuery
} from 'foundation-sites/js/foundation.util.mediaQuery';
import {
    Motion,
    Move
} from 'foundation-sites/js/foundation.util.motion';
import {
    Nest
} from 'foundation-sites/js/foundation.util.nest';
import {
    Timer
} from 'foundation-sites/js/foundation.util.timer';

Foundation.Box = Box;
Foundation.onImagesLoaded = onImagesLoaded;
Foundation.Keyboard = Keyboard;
Foundation.MediaQuery = MediaQuery;
Foundation.Motion = Motion;
Foundation.Move = Move;
Foundation.Nest = Nest;
Foundation.Timer = Timer;

// import { DropdownMenu } from 'foundation-sites/js/foundation.dropdownMenu';
// Foundation.plugin(DropdownMenu, 'DropdownMenu');

const MegaMenu = require('accessible-mega-menu/js/jquery-accessibleMegaMenu')(
    jQuery,
    window,
    document
);

module.exports = {
    $: jQuery,
    IW: Foundation
};