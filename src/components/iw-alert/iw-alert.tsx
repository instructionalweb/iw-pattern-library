import { Component, Prop, Element } from '@stencil/core';

@Component({
  tag: 'iw-alert',
  styleUrl: 'iw-alert.scss'
})
export class IWAlert {
  @Prop() type: string;
  @Prop() close: string;
  @Element() alertElement: HTMLElement;

  handleClose() {
    this.alertElement.querySelector('.iw-alert__content').classList.add('close');
  }

  render() {
    console.log('close button', this.close);
    return (
      <div class={`iw-alert__content ${this.type}`}>
        <slot />
        {this.close === 'true' ? <iw-close onClick={this.handleClose.bind(this)} /> : null}
      </div>
    )
  }
}