import { Component, Element, Prop } from '@stencil/core';
import $ from 'jquery';
import _ from 'lodash';

import { Foundation } from 'foundation-sites/js/foundation.core';
Foundation.addToJquery($);

import { AccordionMenu } from 'foundation-sites/js/foundation.accordionMenu';
Foundation.plugin(AccordionMenu, 'AccordionMenu');

@Component({
  tag: 'iw-accordion-menu',
  styleUrl: 'iw-accordion-menu.scss'
})

export class IWAccordionMenu {
  @Element() accordionMenuElement: HTMLElement;
  @Prop() content: string;

  componentDidLoad() {
    new Foundation.AccordionMenu($(this.accordionMenuElement).children('ul:first-child'), { allowAllClosed: true });
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul class="menu nested">
          {
            _.map(arr, item => {
              return (
                <li>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        _.map(arr, item => {
          return (
            <li>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }

  render() {
    console.log('render accordion menu');
    return (
      <ul class="accordion-menu" data-accordion-menu>
        {this.printList(JSON.parse(this.content), null)}
      </ul>
    );
  }
}