import { Component, Prop, Element, Event, EventEmitter } from '@stencil/core';

import jQuery from 'jquery/dist/jquery.slim';
import _ from 'lodash';

import { Foundation } from 'foundation-sites/js/foundation.core.js';
Foundation.addToJquery(jQuery);

import { DropdownMenu } from 'foundation-sites/js/foundation.dropdownMenu.js';
Foundation.plugin(DropdownMenu, 'DropdownMenu');


@Component({
  tag: 'iw-dropdown-menu',
  styleUrl: 'iw-dropdown-menu.scss'
})
export class IWDropdownMenu {
  @Prop() content: string;
  @Prop() orientation: string;
  @Element() dropdownMenuElement: HTMLElement;
  @Event() dropdownMenuLoaded: EventEmitter;

  classArray = [
    'dropdown',
    'menu'
  ];

  componentWillLoad() {
    this.orientation === 'vertical' ? this.classArray.push(this.orientation) : null;
  }
  componentDidLoad() {
    new Foundation.DropdownMenu(jQuery(this.dropdownMenuElement).children('ul:first-child'), {
      disableHover: true,
      clickOpen: true
    });

    this.dropdownMenuLoaded.emit(true);
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul class="menu nested">
          {
            _.map(arr, item => {
              return (
                <li class={item.current ? "current" : null}>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        _.map(arr, item => {
          return (
            <li class={item.current ? "current" : null}>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }

  render() {
    return (
      <ul class={this.classArray.join(' ')} data-dropdown-menu>
        {this.printList(JSON.parse(this.content), null)}
      </ul>
    );
  }
}