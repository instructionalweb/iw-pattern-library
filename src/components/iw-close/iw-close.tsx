import { Component, Event, EventEmitter } from '@stencil/core';


@Component({
  tag: 'iw-close',
  styleUrl: 'iw-close.scss'
})
export class IWClose {
  @Event() closeBtnLoaded: EventEmitter;

  componentDidLoad() {
    this.closeBtnLoaded.emit(true);
  }

  render() {
    return (
      <button class="close-button" aria-label="Close alert" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    );
  }
}