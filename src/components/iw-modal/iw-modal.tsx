import { Component, Prop, Element } from '@stencil/core';
import { GetYoDigits } from 'foundation-sites/js/foundation.util.core';

@Component({
  tag: 'iw-modal',
  styleUrl: 'iw-modal.scss'
})
export class IWModal {
  @Prop() button: string;
  @Element() modalElement: HTMLElement;

  modalId = GetYoDigits(6, 'modal');

  componentDidLoad() {
  }

  open() {
    this.modalElement.querySelector('.modal').classList.toggle('active');
  }

  close() {
    this.modalElement.querySelector('.modal').classList.remove('active');
  }

  render() {
    return ([
      <button class="button" onClick={this.open.bind(this)}>{this.button}</button>,

      <div class="modal" id={this.modalId} onClick={this.close.bind(this)}>
        <div class="modal__content">
          <slot />
          <iw-close onClick={this.close.bind(this)}></iw-close>
        </div>
      </div>
    ]);
  }
}