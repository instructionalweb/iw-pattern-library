import { Component, Prop } from '@stencil/core';
import _ from 'lodash';

@Component({
  tag: 'iw-breadcrumbs',
  styleUrl: 'iw-breadcrumbs.scss'
})
export class IWBreadcrumbs {
  @Prop() content: string;

  render() {
    let i = 0;
    const arr = JSON.parse(this.content);
    return (
      <nav role="navigation" aria-label="Breadcrumb">
        <ul role="menu" class="breadcrumbs">
          {
            _.map(arr, item => {
              i += 1;
              // last item in list
              if (i === arr.length) {
                return (<li role="menuitem" aria-current="page" class="current">{item.title}</li>)
              }

              // // if url is # and NOT last item
              // if (item.url === '#') {
              //   return (<li class="disabled">{item.title}</li>)
              // }

              // anything else
              return (<li role="menuitem"><a href={item.url}>{item.title}</a></li>)
            })
          }
        </ul>
      </nav>
    );
  }
}