import { Component } from '@stencil/core';

@Component({
  tag: 'iw-docs',
})
export class IWDocs {
  render() {
    return (
      <stencil-router>
        <stencil-route
          url="/"
          routeRender={() => <iw-docs-page doc="index.html" />}
          exact={true}
        />

        <stencil-route
          url="/logo"
          routeRender={() => <iw-docs-page doc="logo.html" />}
        />

        <stencil-route
          url="/breadcrumbs"
          routeRender={() => <iw-docs-page doc="breadcrumbs.html" />}
        />

        <stencil-route
          url="/dropdown-menu"
          routeRender={() => <iw-docs-page doc="dropdown-menu.html" />}
        />

        <stencil-route
          url="/mega-menu"
          routeRender={() => <iw-docs-page doc="mega-menu.html" />}
        />

        <stencil-route
          url="/accordion-menu"
          routeRender={() => <iw-docs-page doc="accordion-menu.html" />}
        />

        <stencil-route
          url="/accordion"
          routeRender={() => <iw-docs-page doc="accordion.html" />}
        />

        <stencil-route
          url="/modal"
          routeRender={() => <iw-docs-page doc="modal.html" />}
        />

        <stencil-route
          url="/alert"
          routeRender={() => <iw-docs-page doc="alert.html" />}
        />

        <stencil-route
          url="/table"
          routeRender={() => <iw-docs-page doc="table.html" />}
        />

        <stencil-route
          url="/icons"
          routeRender={() => <iw-docs-page doc="icons.html" />}
        />

        <stencil-route
          url="/code"
          routeRender={() => <iw-docs-page doc="code.html" />}
        />

      </stencil-router>
    )
  }
}