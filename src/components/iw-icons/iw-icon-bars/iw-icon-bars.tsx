import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'iw-icon-bars'
})
export class IWIconBars {
  @Prop() fill: string;

  render() {
    return (
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
        <path fill={this.fill ? this.fill : ""} d="M1 3h14v3h-14zM1 7h14v3h-14zM1 11h14v3h-14z"></path>
      </svg>
    )
  }
}