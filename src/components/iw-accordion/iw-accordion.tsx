import { Component, Element, State } from '@stencil/core';
// import $ from 'jquery';

// import { Foundation } from 'foundation-sites/js/foundation.core';
// Foundation.addToJquery($);

// import { Accordion, Prop } from 'foundation-sites/js/foundation.accordion';
// Foundation.plugin(Accordion, 'Accordion');

@Component({
  tag: 'iw-accordion',
  styleUrl: 'iw-accordion.scss'
})
export class IWAccordion {
  @Element() accordionElement: HTMLElement;
  @State() paneArray: any[];

  componentWillLoad() {
    console.log('will load', this.accordionElement.children[0].children);
    this.paneArray = Array.from(this.accordionElement.children[0].children);
  }

  componentDidLoad() {
    // const acc = new Foundation.Accordion($(this.accordionElement).children('.accordion'), {
    //   allowAllClosed: true,
    // });
    console.log('did load', this);
  }

  render() {

    return (
      <div class="accordion" role="tablist">
        <slot />
      </div>
    );
  }
}

// <slot />

// {
//   newContent.map((item) => {
//     return (
//       <li class="accordion-item" data-accordion-item>
//         <a href="#" class="accordion-title">{item.title}</a>
//         <div class="accordion-content" data-tab-content innerHTML={item.content}></div>
//       </li>
//     )
//   })
// }




