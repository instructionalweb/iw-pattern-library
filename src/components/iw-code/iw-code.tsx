import { Component, Prop, Element } from '@stencil/core';
import * as Prism from './prism.js';




@Component({
  tag: 'iw-code',
  styleUrl: 'iw-code.scss',
  assetsDir: './'
})
export class IWCode {
  @Prop() mode: string;
  @Element() codeElement: HTMLElement;

  componentDidLoad() {
    const content = this.codeElement.querySelector('code').innerHTML;
    this.codeElement.querySelector('code').innerHTML = content.replace(/^<!---->/, '').replace(/</g, '&lt;').replace(/>/g, '&gt');
    Prism.highlightAll();
  }

  render() {
    return (
      <pre class="line-numbers">
        <code class={`language-${this.mode}`}>
          <slot />
        </code>
      </pre>
    );
  }
}