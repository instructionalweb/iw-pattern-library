import { Component, Prop, State, Element } from '@stencil/core';
import _ from 'lodash';
import { GetYoDigits } from 'foundation-sites/js/foundation.util.core';
// import { TweenMax } from 'gsap';

@Component({
  tag: 'iw-accordion-pane',
  styleUrl: 'iw-accordion-pane.scss'
})
export class IWAccordionPane {
  @Prop() title: string;
  @Element() accordionPaneElement: HTMLElement;
  @State() contentHeight: number;
  @State() flip: number;

  titleElement;
  frameElement;
  contentElement;
  accordionPaneId = GetYoDigits(6, 'accordion');

  show() {
    this.accordionPaneElement.children[0].classList.add('is-active');
    this.titleElement.setAttribute('aria-expanded', 'true');
    this.titleElement.setAttribute('aria-selected', 'true');
    this.frameElement.setAttribute('aria-hidden', 'false');
    // TODO: animate
    // TweenMax.to(this.frameElement, 0.3, {
    //   height: this.contentHeight + 'px'
    // });
    console.log('show');
  }

  hide() {
    this.accordionPaneElement.children[0].classList.remove('is-active');
    this.titleElement.setAttribute('aria-expanded', 'false');
    this.titleElement.setAttribute('aria-selected', 'false');
    this.frameElement.setAttribute('aria-hidden', 'true');
    // TODO: animate
    // TweenMax.to(this.frameElement, 0.3, {
    //   height: 0
    // });
    console.log('hide');
  }

  calculateHeight() {
    // TODO: recalculate on window resize

    // this.contentHeight = this.contentElement.offsetHeight;
    // console.log('height figured');
    // if (this.frameElement.offsetHeight > 1) {
    //   this.show();
    // }
  }

  componentWillLoad() {
    this.flip = -1;
  }

  componentDidLoad() {
    this.contentElement = this.accordionPaneElement.querySelector('.accordion-item__content');
    this.titleElement = this.accordionPaneElement.querySelector('.accordion-item__title');
    this.frameElement = this.accordionPaneElement.querySelector('.accordion-item__frame');

    this.contentHeight = this.contentElement.offsetHeight;
    window.addEventListener('resize', _.debounce(this.calculateHeight, 250));
  }

  handleClick() {
    this.flip = this.flip * -1;

    if (this.flip === 1) {
      this.show();
      return;
    }

    this.hide();
  }

  handleResize() {

  }

  render() {
    return (
      <div class="accordion-item">
        <a
          aria-controls={this.accordionPaneId}
          role="tab"
          id={`${this.accordionPaneId}-label`}
          aria-expanded="false"
          aria-selected="false"
          class="accordion-item__title"
          onClick={this.handleClick.bind(this)}
          href="#"
        >
          {this.title}
        </a>
        <div
          role="tabpanel"
          aria-labelledby={`${this.accordionPaneId}-label`}
          aria-hidden="true"
          id={this.accordionPaneId}
          class="accordion-item__frame"
          data-tab-content
        >
          <div class="accordion-item__content">
            <slot />
          </div>
        </div>
      </div>
    )
  }
}