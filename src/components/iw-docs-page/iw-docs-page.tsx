import { Component, Prop, PropDidChange, State } from '@stencil/core';
import marked from 'marked';
// import Prism from 'prismjs';

@Component({
  tag: 'iw-docs-page',
})
export class IWDocsPage {

  @Prop() doc: string;
  @Prop({ context: 'isServer' }) private isServer: boolean;

  @State() content: string;

  componentWillLoad() {
    return this.fetchNewContent();
  }

  componentDidLoad() {
    // Prism.highlightAll();
  }

  @PropDidChange('doc')
  fetchNewContent() {
    return fetch(`/docs/${this.doc}`)
      .then(response => response.text())
      .then(data => {
        // this.content = marked(data);
        this.content = data;

        const el = document.createElement('div');
        el.innerHTML = data;

        const headerEl = el.querySelector('h1');
        document.title = (headerEl && headerEl.textContent + ' - Stencil') || 'Stencil';

        // requestAnimationFrame is not available for preRendering
        // or SSR, so only run this in the browser
        if (!this.isServer) {
          window.requestAnimationFrame(() => {
            window.scrollTo(0, 0);
          })
        }

      });
  }

  render() {
    return (
      <div innerHTML={this.content}></div>
    )
  }
}