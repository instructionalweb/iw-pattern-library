import { Component, Prop, Element } from '@stencil/core';
import $ from 'jquery/dist/jquery.slim';
import _ from 'lodash';

import MegaMenu from 'accessible-mega-menu/js/jquery-accessibleMegaMenu';
MegaMenu($, window, document);

@Component({
  tag: 'iw-mega-menu',
  styleUrl: 'iw-mega-menu.scss'
})
export class IWMegaMenu {
  @Prop() content: string;
  @Element() megaMenuElement: HTMLElement;

  componentDidLoad() {
    $(this.megaMenuElement).children('nav:first-child').accessibleMegaMenu({
      uuidPrefix: 'iw-menu',
      menuClass: 'iw-menu',
      topNavItemClass: 'iw-menu--item',
      panelClass: 'iw-subnav',
      panelGroupClass: 'iw-subnav--group',
      hoverClass: 'hover',
      focusClass: 'focus',
      openClass: 'open',
      mouseOver: true
    });
  }

  printList(arr, sub) {
    if (sub) {
      return (
        <ul class="menu nested">
          {
            _.map(arr, item => {
              return (
                <li>
                  <a href={item.url}>{item.title}</a>
                  {
                    item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
                  }
                </li>
              )
            })
          }
        </ul>
      )
    } else {
      return (
        _.map(arr, item => {
          return (
            <li>
              <a href={item.url}>{item.title}</a>
              {
                item.submenu && item.submenu.length > 0 ? this.printList(item.submenu, true) : null
              }
            </li>
          )
        })
      )
    }
  }

  render() {
    return (
      <nav class="iw-mega-menu" role="navigation">
        <slot />
      </nav>
    )
  }
}








// import { $ } from '../../js/iw-patterns';

// $('nav.iw-horizontal-menu2').accessibleMegaMenu({
//   /* prefix for generated unique id attributes, which are required 
//      to indicate aria-owns, aria-controls and aria-labelledby */
//   uuidPrefix: 'iw-menu2',

//   /* css class used to define the megamenu styling */
//   menuClass: 'iw-menu2',

//   /* css class for a top-level navigation item in the megamenu */
//   topNavItemClass: 'iw-menu2--item',

//   /* css class for a megamenu panel */
//   panelClass: 'iw-subnav2',

//   /* css class for a group of items within a megamenu panel */
//   panelGroupClass: 'iw-subnav2--group',

//   /* css class for the hover state */
//   hoverClass: 'hover',

//   /* css class for the focus state */
//   focusClass: 'focus',

//   /* css class for the open state */
//   openClass: 'open',

//   /* option for opening the menu on hover */
//   mouseOver: false
// });