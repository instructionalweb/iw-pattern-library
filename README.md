# Instructional Web Pattern Library

> The truth is, design work is *deliberation*.
> It's the pursuit of the best solution to a given problem.
> By relegating design to the realm of visual aesthetics, so much of the web goes undesigned.
> This can only lead to inaccessibility, poor performance and, of course, a general lack of utility.
-- Heydon Pickering, *Inclusive Design Patterns* p.10

## Contributing to This Project


## About This Project

The Instructional Web Pattern Library is a collection of web components to make developing pages and applications easier. Web components are reusable blocks
  of HTML, CSS and JavaScript code. Reusing these blocks allows a more consistent experience for users from one project to the next.

## About Stencil

Web Components are an emerging standard in html. It's simply a way to define your own tags. HTML comes with a variety of tags like
  <code>div</code>,
  <code>blockquote</code>, and
  <code>span</code>, but now we can define our own, more customized elements like
  <code>iw-breadcrumbs</code> or
  <code>iw-tled-logo</code>.

HTML can't recognize these tags since we have created them ourselves, so we have to provide everything from content and appearance to functionality. Each
of our web components has an HTML, CSS and JS aspect of it that the browser uses to load the component.


StencilJS is a set of shorthand functions for making these files a bit easier. The Stencil library takes our scripts and renders out web components for us. 
It also has a convenient library for loading the components in the background on a page.

## Using the Components

To use the components you simply need to include the app.js script in your page.

<pre>
<code class="language-html">
&lt;script src="https:/s3-us-west-2.amazonaws.com/instruction.austincc.edu/components/app.js"&gt;&lt;/script&gt;
</code>
</pre>


Many of our projects have this script already loaded so there isn't anything else you need to do. The app.js script will load the additional component
files as needed, so you just need to start adding components to the page and app.js will take care of the rest.

Each of the components are listed in this menu. To find out how what content and options each component uses, check the individual page for that component.

## Improving the Project

If you want to work on the pattern library you can do that through the git repository. Small typos and edits can be corrected through the Bitbucket website, but if you want to really get your hands dirty you will need to clone the repo and work on the project on your local machine.

<iw-alert type="primary">
  Dont forget to run
  <code>npm install</code> 
  to download all the project dependencies before you start work.
</iw-alert>

After you have cloned the repo, and installed the dependencies you can begin editing the contents of the
<code>src</code> directory. If you want to see your changes in a browser just run
<code>npm start</code> from the terminal and the project will compile and start a live reload server for you. You're off the to races now.
