describe('the page should load', () => {
  it('should have a title', () => {
    cy.visit('/docs/horizontal-menu');
    cy.get('h1').contains('Horizontal Menu');
  });
});

describe('horizontal menu creation', () => {
  it('should load a horizontal menu', () => {
    // go to the page we want to test
    cy.visit('/docs/horizontal-menu');
    // get the ul inside the nav with class iw-horizontal-menu
    // if none is found the test fails
    cy.get('nav.iw-horizontal-menu > ul.iw-menu');
  });
});

describe('submenus should appear on click', () => {
  it('should display a submenu when you click the parent', () => {
    cy.visit('/docs/horizontal-menu');

    cy
      .get('a')
      .contains('Group A')
      .click()
      .should('have.class', 'open');
    cy
      .get('a')
      .contains('Group B')
      .click()
      .should('have.class', 'open');
    cy
      .get('a')
      .contains('Group A')
      .should('not.have.class', 'open');
  });
});

describe('submenus should appear on focus', () => {
  // TODO
  //it('should display a dropdown on submenu focus', () => {});
});
